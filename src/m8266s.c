#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "dht22.h"
#include "serial.h"

#define LEDPORT   PORTC	//!< Port where the leds are attached.
#define LEDDDR    DDRC	//!< DDR for leds.
#define SENSORLED PC3	//!< Led for sensor activity.
#define WIFILED   PC0	//!< Led for wifi activity.
/*! \brief Pin where the dht is connected.
 *
 * Remember to check the dht.h file as there the ports are
 * defined. You must keep both locations consistent.
 */
#define DHTPIN    PB1


/*! \brief Initialise the wifi tcp-server.
 *
 * This function will enable the the multiplexer in the esp8266 and
 * listen to tcp connection on port 80.  It will only return if both
 * commands returned an OK.
 */
void init_wifi_server(void) {
  static const char at_cipmux[] = "AT+CIPMUX=1\r\n";
  static const char at_cipserver[] = "AT+CIPSERVER=1,80\r\n";
  
  uart_puts(at_cipmux);
  while(uart_wait_for_text("OK", 1490000) != 0) {
    LEDPORT ^= _BV(WIFILED);
  }
  uart_puts(at_cipserver);
  while(uart_wait_for_text("OK", 1490000) != 0) {
    LEDPORT ^= _BV(WIFILED);
  }
}


/* \brief Check wifi connectivity.
 *
 * This function checks if the serial connection to the esp8266 works
 * and if an ip address was assigned. It will wait endlessly until
 * this happens.
 */
void check_wifi(void) {
  char buf[DEFBUFLIN];
  short i;
  static const char PROGMEM at_get_ip[] = "AT+CIFSR\r\n";
  char found = 0;

  do {
    strcpy_P(buf, at_get_ip);
    uart_puts(buf);
    do {
      i = uart_gets_with_timeout(buf, sizeof(buf), 2980000UL);
      if(i > 0) {
	if(strncmp("+CIFSR:STAIP", buf, 12) == 0) {
	  found = 1;
	}
      }
    } while(i >= 0);
    _delay_ms(423.5);
    LEDPORT ^= _BV(WIFILED);
  } while(!found);
}


/*! \brief Send data via Wifi.
 *
 * This function will send data via wifi using the 'channel' the AT commands opened for us. It can only send text, NUL bytes terminate the string.
 *
 * \param channel as from AT firmware
 * \param text text to send
 */
void cipsend(unsigned short channel, const char *text) {
  char buf[32];
  
  sprintf(buf, "AT+CIPSEND=%hu,%hu\r\n", channel, strlen(text));
  uart_puts(buf);
  _delay_ms(20); //The datasheet says something about a 20ms interval... Just to be sure.
  uart_puts(text);
  uart_wait_for_text("SEND OK", 1000000UL);
}

/*! Initialise and check sensor.
 *
 */
void check_sensor(struct dht22 *dhtptr) {
  float temp, humi;
  uint8_t ok;

  // We have to wait two seconds anyway... 2017 was the year this
  // project was begun. And it is a prime number.
  _delay_ms(2017);
  // Initialise the sensor pin.
  dht_init(dhtptr, DHTPIN);
  do {
    ok = dht_read_data(dhtptr, &temp, &humi);
    if(!ok) {
      // Blick led to indicate problem.
      LEDPORT |= _BV(SENSORLED);
      _delay_ms(349);
    }
  } while(!ok);
}


int main(void) {
  struct dht22 dht22dt;
  float temp, humi;
  unsigned long bytes_in_channel;
  unsigned short channel;
  int br; //!< Bytes received.
  char buf[40];
  uint8_t ok;

  uart_init();
  // PC3, PC0 as output.
  LEDDDR = _BV(SENSORLED) | _BV(WIFILED);
  // And turn both on, everything else off.
  LEDPORT = _BV(SENSORLED) | _BV(WIFILED);
  // Initialise sensor.
  check_sensor(&dht22dt);
  // Turn off sensor led.
  LEDPORT &= ~(_BV(SENSORLED));
  // Check and initialise the wifi dongle.
  check_wifi();
  init_wifi_server();
  // And turn wifi led off.
  LEDPORT &= ~(_BV(WIFILED));
  // And turn both leds off.
  LEDPORT &= ~(_BV(SENSORLED) | _BV(WIFILED));
  for(;;) {
    LEDPORT ^= _BV(SENSORLED);
    br = uart_gets_with_timeout(buf, sizeof(buf), 2000000UL);
    if((br > 0) && (strncmp(buf, "+IPD", 4) == 0)) {
      //uart_puts(buf);
      LEDPORT |= _BV(WIFILED);
      //Communication to us?
      if(sscanf(buf, "+IPD,%hu,%lu:GET /", &channel, &bytes_in_channel) == 2) {
	cipsend(channel, "HTTP/1.1 200 OK\n\n");
	ok = dht_read_data(&dht22dt, &temp, &humi);
	sprintf(buf, "%2d T %6.2f H %6.2f\n", (int)ok, temp, humi);
	cipsend(channel, buf);
	sprintf(buf, "AT+CIPCLOSE=%hu\r\n", channel);
	uart_puts(buf);
      }
      LEDPORT &= ~(_BV(WIFILED));
    }
  }
  return 0;
}
