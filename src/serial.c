#include "serial.h"
#include <avr/io.h>
#include <string.h>

#define UART_BAUD_RATE 57600
//Formula from Atmel datasheet (attiny, 2010, p 113)
#define UART_BAUD_CALC(UART_BAUD_RATE) ((F_CPU)/((UART_BAUD_RATE)*16l)-1)


/*! \brief initialise serial interface
 *
 * Initialise the USART to 57600 baud 8N1. The same speed must be used
 * on the esp8266. We use 57600 baud and not a higher rate as the
 * error with a 16 Mhz crystal is still quite large and could lead to
 * problems.
 */
void uart_init(void) {
  //asynchronous 8N1
#if defined(__AVR_ATmega88__)
  UBRR0 = UART_BAUD_CALC(UART_BAUD_RATE);
  //Enable transmitter and receiver.
  UCSR0B = (1 << TXEN0) | (1 << RXEN0);
#else
  // set baud rate
  UBRRH = (uint8_t)(UART_BAUD_CALC(UART_BAUD_RATE) >> 8);
  UBRRL = (uint8_t)(UART_BAUD_CALC(UART_BAUD_RATE));
  UCSRB = (1 << TXEN) | (1 << RXEN);
#endif

#ifdef __AVR_ATtiny2313__
  UCSRC = (3 << UCSZ0);
#elif defined(__AVR_ATmega88__)
  // Frame Format: Asynchron 8N1
  UCSR0C = (1 << UCSZ01)|(1 << UCSZ00);
#else
  //URSEL = we access URSRC and not UBRRH (what the heck?), 8N1
  UCSRC = (1 << URSEL) | (3 << UCSZ0);
#endif
}


/*! \brief get a character from serial
 *
 * This function waits indefinitely until a character was received.
 *
 * \return received character
 */
uint8_t uart_getc(void) {
  while (!(UCSR0A & _BV(RXC0))) ;
  return UDR0;
}



/*!\brief output character to serial
 *
 * A single character is output to the serial interface.
 *
 * \param c character to transmit
 */
void uart_putc(unsigned char c) {
#if defined(__AVR_ATmega88__)
  while(!(UCSR0A & (1 << UDRE0)));
  UDR0 = c;
#else
  // wait until UDR ready
  while(!(UCSRA & (1 << UDRE)));
  UDR = c;    // send character
#endif
}


/*!\brief output a string
 *
 * Output a null-terminated string to serial console.
 *
 * \param s string to transmit
 */
void uart_puts(const char *s) {
  // loop until *s != NULL
  while (*s) {
    uart_putc(*s);
    s++;
  }
}

short uart_gets_with_timeout(char *buf, short len, unsigned long timeout) {
  short pos = 0;
  char ch;

  while(pos < len - 1) {
    while (!(UCSR0A & _BV(RXC0))) {
      if(timeout-- == 0) {
	if(pos > 0) {
	  goto leave;
	}
	return -1;
      }
    }
    ch = UDR0;
    switch(ch) {
    case '\r':
      break;
    case '\n':
      goto leave;
    default:
      buf[pos++] = ch;
    }
  }
 leave:
  buf[pos] = '\0';
  return pos;
}


/*! \brief Wait for a text from uart.
 *
 * This is an endless loop which will wait until text was entered. The
 * buffer has a maximum size of 79 characters.
 *
 * \return 0 text was received, 1 timeout was met
 */
int8_t uart_wait_for_text(const char *text, unsigned long timeout) {
  short len;
  char buf[DEFBUFLIN];

  do {
    len = uart_gets_with_timeout(buf, sizeof(buf), timeout);
    if(len >= 0) {
      if(strcmp(text, buf) == 0) {
	return 0;
      }
    }
  } while(len >= 0);
  return 1;
}
