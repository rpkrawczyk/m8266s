#ifndef __SERIAL_H_20180103__
#define __SERIAL_H_20180103__
#include <stdint.h>

#define DEFBUFLIN 80 //!< Default buffer line-size for receiving text from uart.
#define UART_BAUD_RATE 57600


/*! \brief initialise serial interface
 *
 * Initialise the USART to 57600 baud 8N1. The same speed must be used
 * on the esp8266. We use 57600 baud and not a higher rate as the
 * error with a 16 Mhz crystal is still quite large and could lead to
 * problems.
 */
void uart_init(void);


/*! \brief get a character from serial
 *
 * This function waits indefinitely until a character was received.
 *
 * \return received character
 */
uint8_t uart_getc(void);


/*!\brief output character to serial
 *
 * A single character is output to the serial interface.
 *
 * \param c character to transmit
 */
void uart_putc(unsigned char c);


/*!\brief output a string
 *
 * Output a null-terminated string to serial console.
 *
 * \param s string to transmit
 */
void uart_puts(const char *s);


/*!\brief get string from serial with timeout
 *
 * The timeout is specified in loop counts and is dependent on the clock frequency. Maybe later something improved could be used.
 *
 * \param buf pointer to buffer for storing received data
 * \param len buffer length
 * \param timeout number of busy-wait loops
 * \return characters read or -1 on timeout
 */
short uart_gets_with_timeout(char *buf, short len, unsigned long timeout);


/*! \brief Wait for a text from uart.
 *
 * This is an endless loop which will wait until text was entered. The
 * buffer has a maximum size of 79 characters.
 *
 * \return 0 text was received, 1 timeout was met
 */
int8_t uart_wait_for_text(const char *text, unsigned long timeout);


#endif
