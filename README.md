m8266s
======

A simple project to connect an Atmega88 to an ESP8266 and a
DHT22. This is a simple Internet-of-Thing example. The challange will
be to use the unmodified AT-Firmware.

Installation
------------

You have to configure your ESP8266 beforehand. It should connect to an
access point and you must set the serial console to 57600 baud
8N1. Setting the baud rate can be done with the following AT command
`AT+UART_DEF=57600,8,1,0,0`.

Then compile and burn the avr code in the src directory onto the
Atmega88.

![m8266s circuit diagram](m8266-circuit.png "Circuit diagram")

Use the circuit diagram to build a board and enjoy!

Links
-----

 * https://easyeda.com/rpkrawczyk/m8266s-d3d29a3c7c8e463790473f10845dee5f

